var myAssertion = {

  assertError:function(failureMessage)
  {
  	OUT.println("[JMETER_ERROR] [" + sampler.getName() + "] [" + sampler.getUrl() + "], AssertionFailureMessage [" + failureMessage + "]");
  	AssertionResult.setFailureMessage(failureMessage) ;
	AssertionResult.setFailure(true);
  },
  
  assertPass:function(logMessage)
  {
	// OUT.println("[JMETER_PASSED] [" + sampler.getName() + "] " + logMessage);
  },
  
  logDebug:function(logMessage)
  {
	OUT.println("[JMETER_DEBUG] [" + sampler.getName() + "] " + logMessage);
  },
  validateinventory:function(param_inventory, response_inventory)
  {
		//Verifying inventory object now
		for (var key in param_inventory) 
		{
  			var responseValue = response_inventory[key];
			var paramExpectedValue = param_inventory[key];
				
			if(typeof responseValue === 'undefined' || responseValue == null || String(responseValue) !== String(paramExpectedValue))
			{
				myAssertion.assertError("response_inventory does not have valid " + key + ". Received: " + responseValue + ", Expecting: " + paramExpectedValue);
			}
			else
			{
				myAssertion.assertPass("Verifying inventory Key: " + key + ", Expecting: " + paramExpectedValue+", Received: " + responseValue);
			}
  			var itemId = response_inventory['itemId'];
  			if(typeof itemId === 'undefined' || itemId !== null || String(itemId) !== String(paramExpectedValue))
			{
				myAssertion.assertPass("itemId does not have valid " + key + ". Received: " + itemId + ", Expecting: " + paramExpectedValue);
			}
			var itemKey = response_inventory['itemKey'];
  			if(typeof itemKey === 'undefined' || itemKey !== null || String(itemKey) !== String(paramExpectedValue))
			{
				myAssertion.assertPass("itemKey does not have valid " + key + ". Received: " + itemKey + ", Expecting: " + paramExpectedValue);
			}
			var idState = response_inventory['idState'];
  			if(typeof idState === 'undefined' || idState !== null || String(idState) !== String(paramExpectedValue))
			{
				myAssertion.assertPass("typeId does not have valid " + key + ". Received: " + idState + ", Expecting: " + paramExpectedValue);
			}			
		}
			
    	myAssertion.assertPass();
  },
};

var paramsLength = Parameters.length;
var responseCode = prev.getResponseCode();

if (paramsLength == 0)
{
	myAssertion.assertError("Invalid Jmeter test case. Valid JSON as Input param is required for response validation");
}
else if(!responseCode.equals("202"))
{
	myAssertion.assertError("Invalid ResponseCode received. ResponseCode: " + responseCode + ", Expecting: 200");
}
else
{
	myAssertion.logDebug("[" + sampler.getUrl() + "] ====> [" + prev.getResponseDataAsString() + "]");
	
	try
	{
		eval('var parametersJSON = ' + Parameters);
		eval('var apiResponse = ' + prev.getResponseDataAsString());	
	}	 
	catch (e)
	{
		myAssertion.assertError("Response is not a valid JSON Object"+Parameters);
	}
	
	if(typeof apiResponse !== 'undefined' && apiResponse && typeof parametersJSON !== 'undefined' && parametersJSON && !AssertionResult.isFailure())
	{
		var statusJson = apiResponse['status'];
		var code = statusJson['code'];
		var message = statusJson['message'];
		
		if(typeof code === 'undefined' || code == null || code !== 0)
		{
			myAssertion.assertError("Code in JSON response is invalid. Code: " + code + ", Expecting: 0");
		}
		else
		{
			myAssertion.assertPass("Verifying status Code: " + code + ", Expecting: 0");
		}
		
		if(typeof message === 'undefined' || message == null || message !== "Product details as follows")
		{
			myAssertion.assertError("Error message in JSON response is not valid. Message: " + message + ", Expecting: Product details as follows");
		}
		else
		{
			myAssertion.assertPass("Verifying Message: " + message + ", Expecting: Empty");
		}
		//Verify item
		var param_inventory = parametersJSON['productDetailsList'];
		var response_inventory = apiResponse['productDetailsList'];
		if(typeof param_inventory !=='undefined' && param_inventory && typeof response_inventory !=='undefined' && response_inventory)
		{
			myAssertion.validateinventory(param_inventory, response_inventory);
		}
		else
		{
			myAssertion.assertError("One of param or response inventory JSON is not valid. param_inventory: " + param_inventory + ", response_inventory: " + response_inventory);
		}
	}
	else
	{
		failureMessage = "apiResponse is not a valid object. apiResponse: " + apiResponse;
		isAssertionFailed = true;
	}
}