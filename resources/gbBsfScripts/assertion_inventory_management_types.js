var myAssertion = {

  assertError:function(failureMessage)
  {
  	OUT.println("[JMETER_ERROR] [" + sampler.getName() + "] [" + sampler.getUrl() + "], AssertionFailureMessage [" + failureMessage + "]");
  	AssertionResult.setFailureMessage(failureMessage) ;
	AssertionResult.setFailure(true);
  },
  
  assertPass:function(logMessage)
  {
	// OUT.println("[JMETER_PASSED] [" + sampler.getName() + "] " + logMessage);
  },
  
  logDebug:function(logMessage)
  {
	OUT.println("[JMETER_DEBUG] [" + sampler.getName() + "] " + logMessage);
  },
  
  isValidISODate:function(date)
  {
  	var isoFormat = "^(-?(?:[1-9][0-9]*)?[0-9]{4})-(1[0-2]|0[1-9])-(3[0-1]|0[1-9]|[1-2][0-9])T(2[0-3]|[0-1][0-9]):([0-5][0-9]):([0-5][0-9])(\.[0-9]+)?(Z|[+-](?:2[0-3]|[0-1][0-9]):[0-5][0-9])?$";
  	var dateParts = date.match(isoFormat);
    if (dateParts == null) {
    	return false;
	}
	else {
		return true;statusJson
	}
  },
  validatetypes:function(param_types, response_types)
  {
		//Verifying types object now
		for (var key in param_types) 
		{
  			var responseValue = response_types[key];
			var paramExpectedValue = param_types[key];
				
			if(typeof responseValue === 'undefined' || responseValue == null || String(responseValue) !== String(paramExpectedValue))
			{
				myAssertion.assertError("response_types does not have valid " + key + ". Received: " + responseValue + ", Expecting: " + paramExpectedValue);
			}
			else
			{
				myAssertion.assertPass("Verifying types Key: " + key + ", Expecting: " + paramExpectedValue+", Received: " + responseValue);
			}
  			var typeId = response_types['typeId'];
  			if(typeof typeId === 'undefined' || typeId !== null || String(typeId) !== String(paramExpectedValue))
			{
				myAssertion.assertPass("typeId does not have valid " + key + ". Received: " + typeId + ", Expecting: " + paramExpectedValue);
			}
			var typeName = response_types['typeName'];
  			if(typeName !== null || String(typeName) !== String(paramExpectedValue))
			{
				myAssertion.assertPass("typeName does not have valid " + key + ". Received: " + typeName + ", Expecting: " + paramExpectedValue);
			}
		}
			
    	myAssertion.assertPass();
  },

};

var paramsLength = Parameters.length;
var responseCode = prev.getResponseCode();

if (paramsLength == 0)
{
	myAssertion.assertError("Invalid Jmeter test case. Valid JSON as Input param is required for response validation");
}
else if(!responseCode.equals("200"))
{
	myAssertion.assertError("Invalid ResponseCode received. ResponseCode: " + responseCode + ", Expecting: 200");
}
else
{
	myAssertion.logDebug("[" + sampler.getUrl() + "] ====> [" + prev.getResponseDataAsString() + "]");
	
	try
	{
		eval('var parametersJSON = ' + Parameters);
		eval('var apiResponse = ' + prev.getResponseDataAsString());	
	}	 
	catch (e)
	{
		myAssertion.assertError("Response is not a valid JSON Object"+Parameters);
	}
	
	if(typeof apiResponse !== 'undefined' && apiResponse && typeof parametersJSON !== 'undefined' && parametersJSON && !AssertionResult.isFailure())
	{
		var statusJson = apiResponse['status'];
		var code = statusJson['code'];
		var message = statusJson['message'];
		
		if(typeof code === 'undefined' || code == null || code !== 0)
		{
			myAssertion.assertError("Code in JSON response is invalid. Code: " + code + ", Expecting: 0");
		}
		else
		{
			myAssertion.assertPass("Verifying status Code: " + code + ", Expecting: 0");
		}
		
		if(typeof message === 'undefined' || message == null || message !== "Item added")
		{
			myAssertion.assertError("Error message in JSON response is not valid. Message: " + message + ", Expecting: Empty");
		}
		else
		{
			myAssertion.assertPass("Verifying Message: " + message + ", Expecting: Empty");
		}
		
		//Verify types
		var param_types = parametersJSON['types'];
		var response_types = apiResponse['types'];
		if(typeof param_types !=='undefined' && param_types && typeof response_types !=='undefined' && response_types)
		{
			myAssertion.validatetypes(param_types, response_types);
		}
		else
		{
			myAssertion.assertError("One of param or response types JSON is not valid. param_types: " + param_types + ", response_types: " + response_types);
		}
	}
	else
	{
		failureMessage = "apiResponse is not a valid object. apiResponse: " + apiResponse;
		isAssertionFailed = true;
	}
}