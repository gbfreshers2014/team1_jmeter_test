var myAssertion = {

  assertError:function(failureMessage)
  {
  	OUT.println("[JMETER_ERROR] [" + sampler.getName() + "] [" + sampler.getUrl() + "], AssertionFailureMessage [" + failureMessage + "]");
  	AssertionResult.setFailureMessage(failureMessage) ;
	AssertionResult.setFailure(true);
  },
  
  assertPass:function(logMessage)
  {
	// OUT.println("[JMETER_PASSED] [" + sampler.getName() + "] " + logMessage);
  },
  
  logDebug:function(logMessage)
  {
	OUT.println("[JMETER_DEBUG] [" + sampler.getName() + "] " + logMessage);
  },
  
  isValidISODate:function(date)
  {
  	var isoFormat = "^(-?(?:[1-9][0-9]*)?[0-9]{4})-(1[0-2]|0[1-9])-(3[0-1]|0[1-9]|[1-2][0-9])T(2[0-3]|[0-1][0-9]):([0-5][0-9]):([0-5][0-9])(\.[0-9]+)?(Z|[+-](?:2[0-3]|[0-1][0-9]):[0-5][0-9])?$";
  	var dateParts = date.match(isoFormat);
    if (dateParts == null) {
    	return false;
	}
	else {
		return true;statusJson
	}
  },
  validateHistory:function(param_history, response_history)
  {
		//Verifying history object now
		for (var key in param_history) 
		{
  			var responseValue = response_history[key];
			var paramExpectedValue = param_history[key];
				
			if(typeof responseValue === 'undefined' || responseValue == null || String(responseValue) !== String(paramExpectedValue))
			{
				myAssertion.assertError("response_history does not have valid " + key + ". Received: " + responseValue + ", Expecting: " + paramExpectedValue);
			}
			else
			{
				myAssertion.assertPass("Verifying history Key: " + key + ", Expecting: " + paramExpectedValue+", Received: " + responseValue);
			}
  			var userId = response_history['userId'];
  			if(typeof userId === 'undefined' || userId !== null || String(userId) !== String(paramExpectedValue))
			{
				myAssertion.assertPass("userId does not have valid " + key + ". Received: " + userId + ", Expecting: " + paramExpectedValue);
			}
			var assignId = response_history['assignId'];
  			if(typeof assignId === 'undefined' || assignId !== null || String(assignId) !== String(paramExpectedValue))
			{
				myAssertion.assertPass("assignId does not have valid " + key + ". Received: " + assignId + ", Expecting: " + paramExpectedValue);
			}
			var assignStatus = response_history['assignStatus'];
  			if(assignStatus !== null || String(assignStatus) !== String(paramExpectedValue))
			{
				myAssertion.assertPass("assignStatus does not have valid " + key + ". Received: " + assignStatus + ", Expecting: " + paramExpectedValue);
			}
			var itemKey = response_history['itemKey'];
  			if(itemKey !== null || String(itemKey) !== String(paramExpectedValue))
			{
				myAssertion.assertPass("itemKey does not have valid " + key + ". Received: " + itemKey + ", Expecting: " + paramExpectedValue);
			}
			var returnDate = response_history['returnDate'];
  			if(returnDate !== null || String(returnDate) !== String(paramExpectedValue))
			{
				myAssertion.assertPass("returnDate does not have valid " + key + ". Received: " + returnDate + ", Expecting: " + paramExpectedValue);
			}
			var dateAssigned = response_history['dateAssigned'];
  			if(dateAssigned !== null || String(dateAssigned) !== String(paramExpectedValue))
			{
				myAssertion.assertPass("dateAssigned does not have valid " + key + ". Received: " + dateAssigned + ", Expecting: " + paramExpectedValue);
			}
			var itemId = response_history['itemId'];
  			if(itemId !== null || String(itemId) !== String(paramExpectedValue))
			{
				myAssertion.assertPass("itemId does not have valid " + key + ". Received: " + itemId + ", Expecting: " + paramExpectedValue);
			}
		}
			
    	myAssertion.assertPass();
  },

};

var paramsLength = Parameters.length;
var responseCode = prev.getResponseCode();

if (paramsLength == 0)
{
	myAssertion.assertError("Invalid Jmeter test case. Valid JSON as Input param is required for response validation");
}
else if(!responseCode.equals("202"))
{
	myAssertion.assertError("Invalid ResponseCode received. ResponseCode: " + responseCode + ", Expecting: 202");
}
else
{
	myAssertion.logDebug("[" + sampler.getUrl() + "] ====> [" + prev.getResponseDataAsString() + "]");
	
	try
	{
		eval('var parametersJSON = ' + Parameters);
		eval('var apiResponse = ' + prev.getResponseDataAsString());	
	}	 
	catch (e)
	{
		myAssertion.assertError("Response is not a valid JSON Object"+Parameters);
	}
	
	if(typeof apiResponse !== 'undefined' && apiResponse && typeof parametersJSON !== 'undefined' && parametersJSON && !AssertionResult.isFailure())
	{
		var statusJson = apiResponse['status'];
		var code = statusJson['code'];
		var message = statusJson['message'];
		
		if(typeof code === 'undefined' || code == null || code !== 0)
		{
			myAssertion.assertError("Code in JSON response is invalid. Code: " + code + ", Expecting: 0");
		}
		else
		{
			myAssertion.assertPass("Verifying status Code: " + code + ", Expecting: 0");
		}
		
		if(typeof message === 'undefined' || message == null || message !== "Item added")
		{
			myAssertion.assertError("Error message in JSON response is not valid. Message: " + message + ", Expecting: Empty");
		}
		else
		{
			myAssertion.assertPass("Verifying Message: " + message + ", Expecting: Empty");
		}
		
		//Verify history
		var param_history = parametersJSON['history'];
		var response_history = apiResponse['history'];
		if(typeof param_history !=='undefined' && param_history && typeof response_history !=='undefined' && response_history)
		{
			myAssertion.validateHistory(param_history, response_history);
		}
		else
		{
			myAssertion.assertError("One of param or response history JSON is not valid. param_history: " + param_history + ", response_history: " + response_history);
		}
		var param_history = parametersJSON['count'];
		var response_history = apiResponse['count'];
	}
	else
	{
		failureMessage = "apiResponse is not a valid object. apiResponse: " + apiResponse;
		isAssertionFailed = true;
	}
}