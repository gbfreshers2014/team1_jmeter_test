import java.lang.Thread;

String propertyName = "";

if(bsh.args.length > 0)
{
	String bshArg = bsh.args[0];
	
	if (bshArg.equalsIgnoreCase("functional"))
	{
		propertyName = "async_jmeter_thread_wait_time_functional";
	}
	else if (bshArg.equalsIgnoreCase("performance"))
	{
		propertyName = "async_jmeter_thread_wait_time_performance";
	}
	else
	{
		System.out.println("[JMETER_WARN] [" + sampler.getName() + "] Invalid argument for sleep preprocessor script. Please choose one of functional/performance. Current value: " + bshArg);
	}
}

long sleepTime = 5000;
try
{
	sleepTime = Long.parseLong(props.getProperty(propertyName));
}
catch (NumberFormatException e)
{
	System.out.println("[JMETER_WARN] [" + sampler.getName() + "] Jmeter property have invalid thread wait time. Please correct it. [Default: " + sleepTime + "]");
}

System.out.println("[JMETER_INFO] [" + sampler.getName() + "] Sleeping for: " + sleepTime + " mili");
Thread.sleep(sleepTime);
